#!/bin/bash
  
myip=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d:)
response_code=000
for num in {1..5}
do
        for ip in $(arp -al | awk '{print $1}')
        do
                if [ $ip != $myip ]
                then
                        echo "$ip"
                        # curl -s -o /dev/null -I -w "%{http_code}\n" http://$ip:8500/agent/join/$myip
                        response_code=$(curl -m 5 -X PUT -s -o /dev/null -I -w "%{http_code}\n" http://$ip:8500/v1/agent/join/$myip)
                fi
                if [ $response_code = 200 ]
                then
                        echo "joined"
                        break
                fi
                response_code=000
        done
        if [ $response_code = 200 ]
        then
                break
        fi
sleep 5
done