#!/bin/bash
# run like this, set the NOMAD_ADDR environment variable
# NOMAD_ADDR=http://priv-01:4646 ./nomad/nomad_startup.sh
myip=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d:)
config_file_path=/Users/Shared/opt/nomad.d/both.hcl
nomad agent -config $config_file_path -bind $myip
