data_dir = "/Users/Shared/opt/nomad.d"
server {
    enabled = true
    bootstrap_expect = 2
}

client {
    enabled = true
    options {
        "driver.raw_exec.enable" = "1"
    }
}

consul {
    address = "priv-01:8500"
}