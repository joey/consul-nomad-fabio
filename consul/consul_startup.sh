#!/bin/bash
# run like this
# CONSUL_HTTP_ADDR=http://priv-01:8500 ./consul/consul_startup.sh
# data-dir probably should be /var/lib/consul
consul_config_dir="/Users/Shared/consul/conf"
consul agent -config-dir $consul_config_dir
